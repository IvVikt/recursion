<?php

$searchRoot = '/home/ivvikt/NetBeansProjects/recursion';
$searchName = 'test.txt';
$searchResult = [];

$searchExceptions = ['.', '..'];

function searchFile($folderName, $fileName, &$arResult)
{
    global $searchExceptions;
    
    $arFolderResult = scandir($folderName);
    
    foreach ($arFolderResult as $result) {
        $filePath = $folderName . '/' . $result;

        if (in_array($result, $searchExceptions)) {
            continue;
        }
        
        if (is_dir($filePath)) {
            searchFile($filePath, $fileName, $arResult);
        } else {
            if ($result === $fileName) {
                $arResult[] = $filePath;
            }
        }
    }
}

searchFile($searchRoot, $searchName, $searchResult);

$searchResult = array_filter($searchResult, fn ($filePath) => filesize($filePath) > 0);

print_r($searchResult);